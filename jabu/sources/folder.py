import os
import tarfile
import logging

from jabu.sources import backupsource
from jabu.sources.source import BackupSource
from jabu.exceptions import BackupException


@backupsource('folder')
class Folder(BackupSource):
    def __init__(self, config):
        BackupSource.__init__(self, config, "Folder", "tar.gpg")
        self.path = config["path"]
        self.excludes = []
        if 'excludes' in config:
            self.excludes = config['excludes']

    def dump(self):
        tarfilename = '%s/%s.tar.gz' % (self.tmpdir, self.id)
        logging.info("Backing up '%s' (%s)..." % (self.name, self.type))
        tar = tarfile.open(tarfilename, 'w:gz')
        os.chdir(os.path.dirname(self.path))
        # TODO: Cachear por si salta alguna excepcion y mostrarlo por el log
        tar.add(os.path.basename(self.path), recursive=True, filter=lambda tarinfo: None if tarinfo.name in self.excludes else tarinfo)
        tar.close()
        return [tarfilename, ]
