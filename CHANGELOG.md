#Jabu Changelog

This document provides a summary of all notable changes to the jabu by release.
For a detailed view of what’s changed, refer to the repository’s commit history.

This project utilizes semantic versioning.


0.2.2 (2018-01-18)
Initial release.