JABU - Just Another Backup Utility
===========

.. image:: https://img.shields.io/pypi/v/jabu.svg?style=flat-square
  :target: https://pypi.org/project/jabu/
  :alt: Latest PyPI version

.. image:: https://img.shields.io/pypi/pyversions/jabu.svg?style=flat-square
  :target: https://pypi.org/project/jabu/
  :alt: Python versions


**DEVELOPMENT BRANCH**: The current branch is a development version. Go to the stable release by clicking on `the master branch <https://gitlab.com/carbans/jabu/tree/master>`_.

This python script, given a backup configuration, will cycle through a list of backup 'sources'

1. *Install* Jabu

..code:: console
   $ sudo pip install jabu

2. Create a `jabu` user and add to sudoers

..code:: console
   # useradd -m jabu
   # cat > /etc/sudoers.d/99-jabu <<EOF
	jabu ALL=(ALL) NOPASSWD: /bin/tar
     EOF

3. Create config file, example in the directory `config-sample.json`

..code:: console
   # cp -rf jabu/jabu/config-sample.json /home/jabu/mynightlybackup.json

4. Test your configuration by doing initial run of the jabu

..code:: console
   # sudo -H -u backups /usr/local/bin/jabu -v /home/jabu/mynightlybackup.json

5. Add yout job to cron.

..code:: console
   # cat > /etc/cron.d/nightly-backups.conf <<EOF
	0 2 * * * jabu /usr/local/bin/jabu /home/jabu/mynightlybackup.json
     EOF

